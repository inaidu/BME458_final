void setup() {
  // put your setup code here, to run once:
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
}

void loop() {

uint32_t period = 1 * 2000L;       // 5 minutes

for( uint32_t tStart = millis();  (millis()-tStart) < period;  ){
   
  digitalWrite(8, HIGH);
  digitalWrite(7, LOW);
  analogWrite(9, 30);
}
delay(1000);
for( uint32_t tStart = millis();  (millis()-tStart) < period;  ){
   
  digitalWrite(8, LOW);
  digitalWrite(7, HIGH);
  analogWrite(9, 30);
}
  digitalWrite(8, LOW);
  digitalWrite(7, LOW);
 delay(1000000);  //pauses for 100 seconds


}